﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

    public int playerDamage;

    private Animator animator;
    private Transform target;
    private bool skipMove;

	protected override void Start () {
        GameManager.instance.AddEnemyToList(this);
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        base.Start();
	}

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        if(skipMove)
        {
            skipMove = false;
            return;
        }

        base.AttemptMove<T>(xDir, yDir);
        skipMove = true;
    }

    public void MoveEnemy()
    {
        int xDir = 0;
        int yDir = 0;


        //if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
        if((Mathf.Abs(target.position.x - transform.position.x) < Mathf.Abs(target.position.y - transform.position.y)))
        {
            //if the player has a thirst for dead flesh, the enemies will run in the opposite direction
            //changed 'target.GetComponent<Player>().canEatEnemy' to 'Player.canEatEnemy'
            if (Player.canEatEnemy == true)
            {
                yDir = target.position.y > transform.position.y ? -1 : 1;
            }
            else
            {
                yDir = target.position.y > transform.position.y ? 1 : -1;
            }
            
        }
        else
        {
            if(Player.canEatEnemy == true)
            {
                xDir = target.position.x > transform.position.y ? -1 : 1;
            }
            else
            {
                xDir = target.position.x > transform.position.y ? 1 : -1;
            }
        }

        AttemptMove<Player>(xDir, yDir);
    }

    protected override void OnCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;

        if (!Player.canEatEnemy)
        {       
            animator.SetTrigger("enemyAttack");
            hitPlayer.LoseFood(playerDamage);
        }
        else
        {
            hitPlayer.OnTriggerEnter2D(GetComponent<Collider2D>());
        }
    }
}
