﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject {

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public int pointsPerEnemy = 5;
    public int foodSacrifice = 10;
    public float restartLevelDelay = 1f;
    public Text foodText;
    public static bool canEatEnemy;
    public static Text hungerForFleshText;

    private Animator animator;
    private GameObject levelTintImage;
   
    private int food;
    
	// Use this for initialization
	protected override void Start () {
        animator = GetComponent<Animator>();

        levelTintImage = GameObject.Find("TintImage");
        hungerForFleshText = GameObject.Find("FleshText").GetComponent<Text>();

        food = GameManager.instance.playerFoodPoints;

        foodText.text = "Food: " + food;
        //hungerForFleshText.text = "You hunger for flesh...";

        base.Start();
	}
	
    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;
    }

    // Update is called once per frame
    void Update() {
        if (!GameManager.instance.playersTurn)
        {
            return;
        }

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
        {
            vertical = 0;
        }

        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<Wall>(horizontal, vertical);
        }

        if (canEatEnemy)
        {
            levelTintImage.SetActive(true);
        } 
        else
        {
            levelTintImage.SetActive(false);
        }

        /*if(food <= 80)
        {
            canEatEnemy = true;
            levelTintImage.SetActive(true);

        }
        else
        {
            canEatEnemy = false;
            levelTintImage.SetActive(false);
        }*/
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        food--;
        foodText.text = "Food: " + food;

        base.AttemptMove<T>(xDir, yDir);

        //RaycastHit2D hit;

        //if(!Move(xDir, yDir, out hit) && canEatEnemy)
        //{
        //    //hit.rigidbody.GetComponent<Enemy>().gameObject.SetActive(false);
        //}

        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit")
        {
            //levelTintImage.SetActive(false);
            hungerForFleshText.text = "";
            canEatEnemy = false;
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food")
        {
            food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Enemy" && canEatEnemy && other.gameObject.activeSelf)
        {
            food += pointsPerEnemy;
            foodText.text = "+" + pointsPerEnemy + " Food: " + food;
            other.gameObject.SetActive(false);
            //Destroy(other.gameObject);
        }
        else if (other.tag == "PowerUp")
        {
            food -= foodSacrifice;
            //Player must sacrifice food in order to pick up power
            foodText.text = "-" + foodSacrifice + " Food: " + food;
            canEatEnemy = true;
            other.gameObject.SetActive(false);
        }
    }

    protected override void OnCantMove<T>(T component)
    {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
    }

    private void Restart()
    {
        //Application.LoadLevel(Application.loadedLevel);
        SceneManager.LoadScene(0);
    }

    public void LoseFood(int loss)
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();
    }

    private void CheckIfGameOver()
    {
        if(food <= 0)
        {
            GameManager.instance.GameOver();
        }
    }
}
